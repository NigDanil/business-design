[index](../../README.md)

---
<h3>BPMN Diagrams</h3>

- [ ] [Implicit Termination](implicit_termination.md) (New)🔥
- [ ] [MI with a priori Runtime Knowledge Example](mi_with_priori_runtime_knowledge.md) (New)🔥
- [ ] [Deferred Choice](deferred_choice.md) (New)🔥
- [ ] [Canceling Discriminator](canceling_discriminator.md)
- [ ] [Cancel Case](cancel_case.md)
- [ ] [Collaboration](collaboration.md)
- [ ] [Milestone](milestone.md)
- [ ] [Interleaved Routing](interleaved_routing.md)
