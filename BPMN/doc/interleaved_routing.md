[Content](content.md)

---

<h1>Cheat sheets</h1>

<details>
  <summary>Show Cheat sheet BPMN General</summary>

![BPMN General](../data/img/cheat_sheet/BPMN_General_Part_1.png)

</details>
<details>
  <summary>Show Cheat sheet BPMN Task</summary>

![BPMN Task](../data/img/cheat_sheet/BPMN_Tasks.png)

</details>
<details>
  <summary>Show Cheat sheet BPMN Gateways</summary>

![BPMN Gateways](../data/img/cheat_sheet/BPMN_Gateways.png)

</details>

---

<h1>Interleaved Routing</h1>

<details>
  <summary>Show BPMN Diagram</summary>

![Interleaved Routing](../data/img/projects/Interleaved_Routing.jpg)

</details>

<details>
  <summary>Show BPMN Animation Example</summary>

![Interleaved Routing](../data/img/gif/Interleaved_Routing_Example.gif)

</details>
