[Content](content.md)

---

<h1>Cheat sheets</h1>

<details>
  <summary>Show Cheat sheet BPMN General part 1</summary>

![BPMN General part1](../data/img/cheat_sheet/BPMN_General_Part_1.png)

</details>

<details>
  <summary>Show Cheat sheet BPMN General part 2</summary>

![BPMN General part1](../data/img/cheat_sheet/BPMN_General_Part_2.png)

</details>
<details>

<summary>Show Cheat sheet BPMN Task</summary>

![BPMN Task](../data/img/cheat_sheet/BPMN_Tasks.png)

</details>
<details>
  <summary>Show Cheat sheet BPMN Gateways</summary>

![BPMN Gateways](../data/img/cheat_sheet/BPMN_Gateways.png)

</details>

---

<h1>Deferred Choice</h1>

<details>
  <summary>Show BPMN Diagram Pattern</summary>

![Deferred Choice Pattern](../data/img/projects/Deferred_Choice_Pattern.jpg)

</details>

<details>
  <summary>Show BPMN Diagram Examples 1</summary>

![Deferred Choice Examples 1](../data/img/projects/Deferred_Choice_Example_1.jpg)

</details>

<details>
  <summary>Show BPMN Diagram Examples 2</summary>

![Deferred Choice Examples 2](../data/img/projects/Deferred_Choice_Example_2.jpg)

</details>

<details>
  <summary>Show BPMN Animation Pattern</summary>

![Deferred Choice Pattern](../data/img/gif/Deferred_Choice_Pattern.gif)

</details>

<details>
  <summary>Show BPMN Animation Example 1</summary>

![Deferred Choice Examples 1](../data/img/gif/Deferred_Choice_Example_1.gif)

</details>
<details>
  <summary>Show BPMN Animation Example 2</summary>

![Deferred Choice Examples 2](../data/img/gif/Deferred_Choice_Example_2.gif)

</details>
