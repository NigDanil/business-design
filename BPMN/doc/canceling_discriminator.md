[Content](content.md)

---

<h1>Cheat sheets</h1>

<details>
  <summary>Show Cheat sheet BPMN General</summary>

![BPMN General](../data/img/cheat_sheet/BPMN_General_Part_1.png)

</details>
<details>
  <summary>Show Cheat sheet BPMN Task</summary>

![BPMN Task](../data/img/cheat_sheet/BPMN_Tasks.png)

</details>
<details>
  <summary>Show Cheat sheet BPMN Gateways</summary>

![BPMN Gateways](../data/img/cheat_sheet/BPMN_Gateways.png)

</details>

---

<h1>Canceling Discriminator</h1>

<details>
  <summary>Show BPMN Diagram</summary>

![Canceling Discriminator](../data/img/projects/Canceling_Discriminator.jpg)

</details>

<details>
  <summary>Show BPMN Animation</summary>

![Canceling Discriminator](../data/img/gif/Canceling_Discriminator.gif)

</details>
