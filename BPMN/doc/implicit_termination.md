[Content](content.md)

---

<h1>Cheat sheets</h1>

<details>
  <summary>Show Cheat sheet BPMN General</summary>

![BPMN General](../data/img/cheat_sheet/BPMN_General_Part_1.png)

</details>
<details>
  <summary>Show Cheat sheet BPMN Task</summary>

![BPMN Task](../data/img/cheat_sheet/BPMN_Tasks.png)

</details>
<details>
  <summary>Show Cheat sheet BPMN Gateways</summary>

![BPMN Gateways](../data/img/cheat_sheet/BPMN_Gateways.png)

</details>

---

<h1>Implicit Termination</h1>

<details>
  <summary>Show BPMN Diagram Pattern</summary>

![Implicit Termination](../data/img/projects/Implicit_Termination.jpg)

</details>
<details>
  <summary>Show BPMN Diagram Example 1</summary>

![Implicit Termination](../data/img/projects/Implicit_Termination_Example_1.jpg)

</details>
<details>
  <summary>Show BPMN Diagram Example 2</summary>

![Implicit Termination](../data/img/projects/Implicit_Termination_Example_2.jpg)

</details>

<details>
  <summary>Show BPMN Animation Pattern</summary>

![Implicit Termination](../data/img/gif/Implicit_Termination_Pattern.gif)

</details>

<details>
  <summary>Show BPMN Animation Example 1</summary>

![Implicit Termination](../data/img/gif/Implicit_Termination_Example_1.gif)

</details>

<details>
  <summary>Show BPMN Animation Example 2</summary>

![Implicit Termination](../data/img/gif/Implicit_Termination_Example_2.gif)

</details>