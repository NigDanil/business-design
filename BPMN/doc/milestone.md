[Content](content.md)

---

<h1>Cheat sheets</h1>

<details>
  <summary>Show Cheat sheet BPMN General part 1</summary>

![BPMN General part1](../data/img/cheat_sheet/BPMN_General_Part_1.png)

</details>

<details>
  <summary>Show Cheat sheet BPMN General part 2</summary>

![BPMN General part1](../data/img/cheat_sheet/BPMN_General_Part_2.png)

</details>
<details>

<summary>Show Cheat sheet BPMN Task</summary>

![BPMN Task](../data/img/cheat_sheet/BPMN_Tasks.png)

</details>
<details>
  <summary>Show Cheat sheet BPMN Gateways</summary>

![BPMN Gateways](../data/img/cheat_sheet/BPMN_Gateways.png)

</details>

---

<h1>Milestone</h1>

<details>
  <summary>Show BPMN Diagram</summary>

![Milestone](../data/img/projects/Milestone.jpg)

</details>

<details>
  <summary>Show BPMN Animation Pattern</summary>

![Milestone](../data/img/gif/Milestone_Pattern.gif)

</details>

<details>
  <summary>Show BPMN Animation Example</summary>

![Milestone](../data/img/gif/Milestone_Example.gif)

</details>

