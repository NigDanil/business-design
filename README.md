<h1>Business Process Model and Notation</h1>

<p>Business Process Model and Notation (BPMN) is a standard for business process modeling that provides a graphical notation for specifying business processes in a Business Process Diagram (BPD), based on a flowcharting technique very similar to activity diagrams from Unified Modeling Language (UML). The objective of BPMN is to support business process management, for both technical users and business users, by providing a notation that is intuitive to business users, yet able to represent complex process semantics. The BPMN specification also provides a mapping between the graphics of the notation and the underlying constructs of execution languages, particularly Business Process Execution Language (BPEL).</p>

<p>BPMN has been designed to provide a standard notation readily understandable by all business stakeholders, typically including business analysts, technical developers and business managers. BPMN can therefore be used to support the generally desirable aim of all stakeholders on a project adopting a common language to describe processes, helping to avoid communication gaps that can arise between business process design and implementation.</p>

<p>BPMN is one of a number of business process modeling language standards used by modeling tools and processes. While the current variety of languages may suit different modeling environments, there are those who advocate for the development or emergence of a single, comprehensive standard, combining the strengths of different existing languages. It is suggested that in time, this could help to unify the expression of basic business process concepts (e.g., public and private processes, choreographies), as well as advanced process concepts (e.g., exception handling, transaction compensation).</p>

<p>Two new standards, using a similar approach to BPMN have been developed, addressing case management modeling (Case Management Model and Notation) and decision modeling (Decision Model and Notation).</p>

<h3>BPMN Diagrams</h3>

- [BPMN Collections](./BPMN/doc/content.md)

---

<h3>Clone Project</h3>

- [ ] Clone with SSH

```bash
git@gitlab.com:NigDanil/business-design.git
```

- [ ] Clone with HTTPS

```bash
https://gitlab.com/NigDanil/business-design.git
```