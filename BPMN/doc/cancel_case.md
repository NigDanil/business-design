[Content](content.md)

---

<h1>Cheat sheets</h1>

<details>
  <summary>Show Cheat sheet BPMN General</summary>

![BPMN General](../data/img/cheat_sheet/BPMN_General_Part_1.png)

</details>
<details>
  <summary>Show Cheat sheet BPMN Task</summary>

![BPMN Task](../data/img/cheat_sheet/BPMN_Tasks.png)

</details>
<details>
  <summary>Show Cheat sheet BPMN Gateways</summary>

![BPMN Gateways](../data/img/cheat_sheet/BPMN_Gateways.png)

</details>

---

<h1>Cancel Case</h1>

<details>
  <summary>Show BPMN Diagram</summary>

![Cancel Case](../data/img/projects/Cancel_Case.jpg)

</details>

<details>
  <summary>Show BPMN Animation Pattern</summary>

![Cancel Case](../data/img/gif/Cancel_Case_Pattern.gif)

</details>

<details>
  <summary>Show BPMN Animation Example</summary>

![Cancel Case](../data/img/gif/Cancel_Case_Example.gif)

</details>