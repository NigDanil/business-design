[Content](content.md)

---

<h1>Cheat sheets</h1>

<details>
  <summary>Show Cheat sheet BPMN General</summary>

![BPMN General](../data/img/cheat_sheet/BPMN_General_Part_1.png)

</details>
<details>
  <summary>Show Cheat sheet BPMN Task</summary>

![BPMN Task](../data/img/cheat_sheet/BPMN_Tasks.png)

</details>
<details>
  <summary>Show Cheat sheet BPMN Gateways</summary>

![BPMN Gateways](../data/img/cheat_sheet/BPMN_Gateways.png)

</details>

---

<h1>MI with priori Runtime Knowledge</h1>

<details>
  <summary>Show BPMN Diagram Pattern</summary>

![MI with priori Runtime Knowledge](../data/img/projects/MI_with_priori_Runtime_Knowledge.jpg)

</details>

<details>
  <summary>Show BPMN Diagram Example</summary>

![MI with priori Runtime Knowledge](../data/img/projects/MI_with_priori_Runtime_Knowledge_Example.jpg)

</details>

<details>
  <summary>Show BPMN Animation Pattern</summary>

![MI with priori Runtime Knowledge](../data/img/gif/MI_with_priori_Runtime_Knowledge_Pattern.gif)

</details>

<details>
  <summary>Show BPMN Animation Example</summary>

![MI with priori Runtime Knowledge](../data/img/gif/MI_with_priori_Runtime_Knowledge_Example.gif)

</details>

